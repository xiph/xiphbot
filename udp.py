#!/usr/bin/python
import sys
import socket

UDP_IP="127.0.0.1"
UDP_PORT=61001

if (len(sys.argv) and len(sys.argv[1])>0):
  sock = socket.socket( socket.AF_INET, 
                        socket.SOCK_DGRAM ) 
  sock.sendto( sys.argv[1], (UDP_IP, UDP_PORT) )
