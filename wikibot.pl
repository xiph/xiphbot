#!/usr/bin/perl

use warnings;
use strict;

use POE;
use IO::Socket::INET;
use POE::Component::IRC;

use constant DATAGRAM_MAXLEN => 1024;

select((select(STDOUT), $|=1)[0]);

my @channels = ('#daala', '#opus', '#theora', '#xiph');

# Create the component that will represent an IRC network.
my ($irc) = POE::Component::IRC->spawn;

# Create the bot session.  The new() call specifies the events the bot
# knows about and the functions that will handle those events.
POE::Session->create(
    inline_states => {
        _start     => \&bot_start,
        irc_001    => \&on_connect,
        irc_public => \&on_public,
    },
);

POE::Session->create(
    inline_states => {
        _start       => \&server_start,
        get_datagram => \&server_read,
    }
);

$poe_kernel->run;

exit;


# UDP Server
sub server_start {
    my $kernel = $_[KERNEL];

    my $socket = IO::Socket::INET->new(
        Proto     => 'udp',
        LocalHost => '127.0.0.1',
        LocalPort => 61001,
    );

    die "Couldn't create server socket: $!" unless $socket;
    $kernel->select_read( $socket, "get_datagram" );
}

sub server_read {
    my ( $kernel, $socket ) = @_[ KERNEL, ARG0 ];
    my $ircmessage = "";
    recv( $socket, my $message = "",  DATAGRAM_MAXLEN, 0 );
#    $message =~ /\[\[(.+)\]\].+03(.+)5\*.+10(.+)$/s;
#    $ircmessage = "$1 by $2 ($3)";
    my $clean = $message;
    $clean =~ s/#\w+//g;
    my $handled = 0;
    foreach my $channel (@channels) {
        if ($message =~ /$channel/) {
            $irc->yield( privmsg => $channel, $clean );
            $handled = 1;
        }
    }
    $irc->yield( privmsg => "#xiph", $message ) unless $handled;
}



# IRC Server

# The bot session has started.  Register this bot with the "magnet"
# IRC component.  Select a nickname.  Connect to a server.
sub bot_start {
    my $kernel  = $_[KERNEL];
    my $heap    = $_[HEAP];
    my $session = $_[SESSION];

    $irc->yield( register => "all" );

    my $nick = 'XiphWiki';
    $irc->yield(
        connect => {
            Nick     => $nick,
            Username => 'XiphWiki',
            Ircname  => 'Xiph.Org wiki changes',
            Server   => 'irc.freenode.org',
            Port     => '6667',
        }
    );
}

# The bot has successfully connected to a server.  Join a channel.
sub on_connect {
    foreach my $channel (@channels) {
      $irc->yield( join => $channel );
    }
}

# The bot has received a public message.  Parse it for commands, and
# respond to interesting things.
sub on_public {
#    my ( $kernel, $who, $where, $msg ) = @_[ KERNEL, ARG0, ARG1, ARG2 ];
#    my $nick = ( split /!/, $who )[0];
#    my $channel = $where->[0];

#    my $ts = localtime;
#    print " [$ts] <$nick:$channel> $msg\n";

#    if ( my ($rot13) = $msg =~ /^rot13 (.+)/ ) {
#        $rot13 =~ tr[a-zA-Z][n-za-mN-ZA-M];
        # Send a response back to the server.
#        $irc->yield( privmsg => "#ChekMate", $rot13 );
#    }
}

