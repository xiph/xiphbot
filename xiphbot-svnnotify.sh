#!/bin/sh

REPOS=$1
REV=$2

if test -z "$REPOS"; then
  REPOS=/srv/svn/xiph
fi
if test -z "$REV"; then
  REV=$(svnlook youngest $REPOS)
fi

# look up author and log message
AUTHOR=$(svnlook author -r $REV $REPOS)
LOG=$(svnlook log -r $REV $REPOS | head -1)

# ansi colour escapes
reset="\x1b[0m"
green="\x1b[32m"
yellow="\x1b[33m"
blue="\x1b[34m"
magenta="\x1b[35m"
cyan="\x1b[36m"

# construct the message
msg="${yellow}svn${reset} ${green}$AUTHOR${reset} $LOG https://trac.xiph.org/changeset/$REV"

# send the message to irc
#echo -e $msg
udp.py "$(echo -e $msg)"
