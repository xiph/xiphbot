This is a set of scripts we use for IRC notification.

MediaWiki has a feature where it can generate a udp packet
per change, to be routed as you like. We started using this
to track changes to our wiki, and then when CIA.vc when
down we started using it for commit notifications as well.

For commit notifications we call udp.py from a hook script.

E.g. from svn/hooks/post-commit:

/bin/sh /path/to/xiphbot-svnnotify.sh "$REPOS" "$REV"

And from git/hooks/update:

refname="$1"
oldrev="$2"
newrev="$3"
/bin/sh /path/to/xiphbot-gitnotify.sh ${refname} ${oldrev} ${newrev}
